/**
 * Created by nitrosalat on 14.11.15.
 */
var fs = require('fs');
var xml = require('xml');

var capabilitesManager = require('./GetCapabilitiesManager');
var describeCoverageManager = require('./DescribeCoverageManager');
var app = require('./../app');

//var utils = require('./Utils.js');
//
//
//var writer = fs.createWriteStream('extracted.bil', {flags: 'w',encoding:'binary'});
//writer.on('finish', function() {
//    console.log("final");
//});
//var reader = fs.createReadStream('/home/nitrosalat/projects/CoverageServer/coverages/LE71850182002192SGS00_B1.bil')
//var buffer;
//var totalLength = 0;
//var chunks = [];
//reader.on('data',function(data){
//    totalLength += data.length
//    //buffer = new Buffer(data.length);
//    //console.time('time')
//    chunks.push(data);
//    //console.timeEnd('time')
//    //writer.write(buffer);
//
//});
//reader.on('end', function () {
//    console.time('time')
//    buffer = Buffer.concat(chunks,totalLength);
//    console.timeEnd('time')
//    console.time('extract');
//    var extracted = utils.extract(buffer,7551,8141,1,0,0,4000,4000);
//    console.timeEnd('extract')
//    console.log(extracted.length);
//    writer.write(extracted);
//    writer.end();
//});

function getCoverage(request)
{
    console.log(request.query)
    var identifier = request.query.identifier;
    var coverageFilePath = request.protocol + '://' + request.get('host') + '/WCS/coverages/' + identifier + '.' + request.query.format.toLowerCase();
    var headerFilePath = request.protocol + '://' + request.get('host') + '/WCS/headers/' + identifier + '.hdr';
    var coverageJSONObject = {
        Coverage : [
            {Title : identifier},
            {Identifier : identifier},
            {Abstract : 'null'},
            {Reference : {_attr : {
                "href" : coverageFilePath,
                role : 'coverage'
            }}},
            {Reference : {_attr : {
                "href" : headerFilePath,
                role : 'metadata'
            }}}

        ]
    };
    var coveragesJSON = {
        Coverages : [coverageJSONObject]
    };
    console.log(xml(coveragesJSON,{ declaration: true }))
    return xml(coveragesJSON,{ declaration: true });
}
module.exports.getCoverage = getCoverage;