/**
 * Created by nitrosalat
 */
var xml = require('xml');
var fs = require('fs');
var xml2js = require('xml2js');
var uuid = require('uuid')
var describeCoverageManager = require('./DescribeCoverageManager')
var coverages;
var coveragesPath;

var contents = [];
var response;
var xmlResponse;

function getResponse(query)
{
    return xmlResponse;
}

module.exports = function(coveragesPath){
    if(!coveragesPath) coveragesPath = './coverages/';

    coverages = fs.readdirSync(coveragesPath);
    coverages.forEach(function (item) {
        var name = item.replace(/\.[^/.]+$/, "");//remove filename extension
        contents.push(
            {CoverageSummary : [
                {Identifier : name},
                {Title : name}
            ]}
        );
    });

    response = {
        Capabilities:[
            {_attr : {
                version : "1.1.2"
            }},
            {ServiceIdentification :  [
                {Title : "SPBU"},
                {ServiceType : "WCS"},
                {ServiceTypeVersion : "1.1.2"}
            ]},
            {Contents : contents}
        ]
    };
    xmlResponse = xml(response, { declaration: true });

    return {
        getResponse : getResponse
    };
}