/**
 * Created by nitrosalat on 14.11.15.
 */

var fs = require('fs');
var xml = require('xml');
var headersDir;
var headersCollection = {};

var coveragesObjectsCache = {};




function getXMLDescription(query)
{
    var identifier = query.identifiers;
    var headerObj = headersCollection[identifier];
    console.log(headerObj)
    var jsonObject = {
        CoverageDescription : [
            {Title : identifier},
            {Identifier : identifier},
            {Abstract : "null"},
            {Domain : [
                {SpatialDomain : [
                    {BoundingBox : [
                        {_attr : {
                            crs : "UTM",
                            dimensions : "2"
                        }},
                        {UpperCorner : (parseInt(headerObj.ULXMAP)+parseInt(headerObj.NCOLS)*parseInt(headerObj.XDIM)).toString() + ' ' +headerObj.ULYMAP},
                        {LowerCorner : headerObj.ULXMAP + ' ' +(parseFloat(headerObj.ULYMAP)-parseInt(headerObj.NROWS)*parseInt(headerObj.YDIM)).toString() }
                    ]}
                ]}
            ]},
            {Range : [

            ]},
            {SupportedCRS : "UTMZone35N"},
            {SupportedFormat : "BIP"},
            {SupportedFormat : "BIL"},
            {SupportedFormat : "BSQ"}
        ]
    };
    var descriptions = {
        CoverageDescriptions : [jsonObject]
    };
    console.log(xml(descriptions,{ declaration: true }))
    return xml(descriptions,{ declaration: true });
}

module.exports = function(metadataPath){
    if(!metadataPath) metadataPath = './metadata/';
    headersDir = fs.readdirSync(metadataPath);

    headersDir.forEach(function (item) {
        var headerFile = fs.readFileSync(metadataPath + item);
       // var filenameExtension = item.split('.').pop();
        var name = item.replace(/\.[^/.]+$/, "");
        var str = headerFile.toString();
        var header = {};
        str.split('\n').forEach(function (x) {
            var arr = x.split(/ +/);
            arr[1] && (header[arr[0]] = arr[1]);
        });
        headersCollection[name] = header;
    });

    console.log(headersCollection)

    return {
        getXMLDescription : getXMLDescription,
        headers : headersCollection,
        coveragesObjectsCache : coveragesObjectsCache,
    }
}

